package person.search

import person.types.Person
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SearchRepository(private val searchView: SearchView, private val personBackend: PersonBackend) {
    companion object {
        private const val MIN_LENGTH = 3
        private const val DEBOUNCE_MILLIS = 300L
    }

    fun search(): Observable<List<Person>> {

        /**
         * Implement the search call according the following criteria:
         * - The search query string must contain at least 3 characters
         * - To save backend traffic: only search if search query hasn't changed within the last 300 ms
         * - If the user is typing fast "Hannes" and than deletes and types "Hannes" again (exceeding 300 ms) the search should not execute twice.
         */

        return searchView.onSearchTextchanged()
                .filter { it.length > MIN_LENGTH }
                .debounce(DEBOUNCE_MILLIS, TimeUnit.MILLISECONDS)
                .distinct()
                .flatMap { personBackend.searchfor(it) }
                .filter { !it.isEmpty() }
    }
}
