package person.favorites

import person.types.PersonWithAddress
import io.reactivex.Observable
import io.reactivex.functions.BiFunction

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {
        data class PersonAndFavorites(val people: List<PersonWithAddress>, val favorites: Set<Int>)

        val allPersons = personBackend.loadAllPersons()
        val favorites = favoritesDatabase.favoriteContacts()

        return favorites.flatMap { favs -> allPersons.map { PersonAndFavorites(it, favs) } }
                .map { paf -> paf.people.filter { it.person.id in paf.favorites } }
    }
}
