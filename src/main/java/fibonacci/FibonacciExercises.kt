package fibonacci

import io.reactivex.Observable

class FibonacciExercises {


    fun fibonacci(n: Int): Observable<Int> {
        data class FibPair(val prev: Int, val cur: Int)
        return Observable.just(0)
                .repeat()
                .scan(FibPair(0, 1)) { a, _ -> FibPair(a.cur, a.prev + a.cur) }
                .map { it.prev }
                .take(n.toLong())
    }
}
